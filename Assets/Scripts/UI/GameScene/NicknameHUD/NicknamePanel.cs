using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class NicknamePanel : MonoBehaviour
{
	[Header("# NicknameHUD Prefab")][SerializeField]
	private NicknameHUD _NicknameHUDPrefab;

	/// <summary>
	/// 닉네임 HUD 를 생성합니다.
	/// </summary>
	/// <param name="userNickname">유저 닉네임을 전달합니다.</param>
	/// <returns>생성된 닉네임 HUD 객체를 반환합니다.</returns>
	public NicknameHUD CreateNicknameHUD(string userNickname)
	{
		NicknameHUD newNicknameHUD = Instantiate(_NicknameHUDPrefab, transform);
		newNicknameHUD.SetNickname(userNickname);

		return newNicknameHUD;
	}

}
