using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NicknameHUD : MonoBehaviour
{
	[Header("# 닉네임 텍스트")]
	[SerializeField]
	private TMP_Text _NicknameText;

	public void SetNickname(string userNickname)
	{
		_NicknameText.text = userNickname;
	}



}
