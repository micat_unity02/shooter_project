using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public sealed class GameSceneUI : MonoBehaviour
{
	private NicknamePanel _NicknamePanel;

	/// <summary>
	/// 현재 마우스 위치
	/// </summary>
	private Vector2 _CurrentCursorPosition;

	/// <summary>
	/// 화면에 표시되는 플레이어 캐릭터 위치
	/// </summary>
	private Vector2 _PlayerCharacterPositionInScreen;

	/// <summary>
	/// 마우스를 바라보는 방향을 나타냅니다.
	/// </summary>
	public Vector3 lookDirection { get; private set; }

	/// <summary>
	/// 닉네임 패널
	/// </summary>
	public NicknamePanel nicknamePanel => _NicknamePanel ??
		(_NicknamePanel = GetComponentInChildren<NicknamePanel>());

	private void Update()
	{
		Vector2Control cursorPos = Mouse.current.position;
		_CurrentCursorPosition = new Vector2(cursorPos.value.x, cursorPos.value.y);

		Vector2 lookDirectionInScreen = _CurrentCursorPosition - _PlayerCharacterPositionInScreen;
		lookDirectionInScreen.Normalize();

		lookDirection = new Vector3(
			lookDirectionInScreen.x, 0.0f, lookDirectionInScreen.y);
	}

	public void UpdatePlayerCharacterPosition(Vector3 playerCharacterPosition)
	{
		_PlayerCharacterPositionInScreen = Camera.main.WorldToScreenPoint(playerCharacterPosition);
	}










}
