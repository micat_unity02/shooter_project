using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Search;
using UnityEngine;
using UnityEngine.UI;

public sealed class CreateAccountPanel : MonoBehaviour
{
	[Header("# 입력 필드")]
	public TMP_InputField m_IDInputField;
	public TMP_InputField m_PWInputField;
	public TMP_InputField m_NicknameInputField;

	[Header("# 버튼")]
	public Button m_CreateAccountButton;
	public Button m_CancelButton;

	[Header("# 로그인 패널")]
	public GameObject m_LoginPanel;

	[Header("# 입력 블로킹용 패널")]
	public GameObject m_BlockingPanel;
	public TMP_Text m_PanelText;




	private void Awake()
	{
		// 회원가입 패널 비활성화
		gameObject.SetActive(false);

		// 버튼 이벤트 바인딩
		m_CreateAccountButton.onClick.AddListener(OnCreateAccountButtonClicked);
		m_CancelButton.onClick.AddListener(OnCancelButtonClicked);

		m_BlockingPanel.SetActive(false);
	}

	private void OnEnable()
	{
		NetworkManager.instance.AddPacketEvent(
			PacketType.CreateAccountResponse, OnCreateAccountResponsed);
	}

	private void OnDisable()
	{
		NetworkManager.instance.RemovePacketEvent(
			PacketType.CreateAccountResponse, OnCreateAccountResponsed);
	}

	private void OnCreateAccountResponsed(PacketType packetType, object data)
	{
		// 응답 결과를 얻습니다.
		SimpleResponsePacket response = (SimpleResponsePacket)data;

		Dispatcher.Enqueue(() =>
		{
			// 계정 생성 성공
			if (response.result)
				m_PanelText.text = "계정 생성 성공.\n로그인 화면으로 이동합니다.";
			else m_PanelText.text = $"계정 생성 실패.\n{response.context}";
		});

		// 2초 뒤 실행
		Dispatcher.Enqueue(() =>
		{
			m_BlockingPanel.SetActive(false);

			if (response.result) OnCancelButtonClicked();
		}, 2.0f);


	}

	private void OnCreateAccountButtonClicked()
	{
		string id = m_IDInputField.text;
		string pw = m_PWInputField.text;
		string nickname = m_NicknameInputField.text;

		// 요청시킬 데이터를 생성합니다.
		CreateAccountRequestPacket createAccountRequest = new(id, pw, nickname);

		// 계정 생성 요청 실패 여부
		bool failed = false;

		// 금칙어 확인을 위한 리스트
		List<string> forbiddenWords;

		// 입력된 문자열이 계정 생성이 불가능한 문자열인지 확인합니다.
		if (!createAccountRequest.IsRegisterable(out string context))
			failed = true;

		// ID에 금칙어가 포함되어있는지 확인합니다.
		else if (ForbiddenWords.Exist(id, out forbiddenWords))
		{
			failed = true;

			context = $"ID에 해당 글자를 포함시킬 수 없습니다.\n";
			foreach (string forbiddenWord in forbiddenWords)
				context += $"{forbiddenWord} ";
		}

		// Nickname에 금칙어가 포함되어있는지 확인합니다.
		else if (ForbiddenWords.Exist(nickname, out forbiddenWords))
		{
			failed = true;

			context = $"닉네임에 해당 글자를 포함시킬 수 없습니다.\n";
			foreach (string forbiddenWord in forbiddenWords)
				context += $"{forbiddenWord} ";
		}
		// 가입 요청이 가능한 경우
		else context = "계정 생성 요청중...";

		// 상태 표시
		m_PanelText.text = context;
		m_BlockingPanel.SetActive(true);


		if (failed)
		{

		}
		else
		{
			// 계정 생성 요청
			NetworkManager.instance.SendPacket(Core.PacketType.CreateAccountRequest, createAccountRequest);
		}
	}

	private void OnCancelButtonClicked()
	{
		// 회원가입 패널 비활성화
		gameObject.SetActive(false);

		// 로그인 패널 활성화
		m_LoginPanel.gameObject.SetActive(true);
	}
}
