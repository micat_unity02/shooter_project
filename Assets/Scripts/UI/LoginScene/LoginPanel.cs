using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Search;
using UnityEngine;
using UnityEngine.UI;

public sealed class LoginPanel : MonoBehaviour
{
	[Header("# 입력 필드")]
	public TMP_InputField m_IDInputField;
	public TMP_InputField m_PWInputField;

	[Header("# 버튼")]
	public Button m_LoginButton;
	public Button m_CreateAccountButton;

	[Header("# 회원가입 패널")]
	public GameObject m_CreateAccountPanel;

	[Header("# 입력 블로킹용 패널")]
	public GameObject m_BlockingPanel;
	public TMP_Text m_PanelText;


	private void Awake()
	{
		// 버튼 이벤트 바인딩
		m_LoginButton.onClick.AddListener(OnLoginButtonClicked);
		m_CreateAccountButton.onClick.AddListener(OnCreateAccountButtonClicked);
	}

	private void OnEnable()
	{
		NetworkManager.instance.AddPacketEvent(
			PacketType.LoginResponse, OnLoginResponsed);
	}

	private void OnDisable()
	{
		NetworkManager.instance.RemovePacketEvent(
			PacketType.LoginResponse, OnLoginResponsed);
	}

	private void OnLoginResponsed(PacketType packetType, object data)
	{
		LoginResponsePacket response = (LoginResponsePacket)data;

		Dispatcher.Enqueue(() =>
		{
			// 로그인 성공
			if (response.result) m_PanelText.text = "로그인 성공!";
			// 로그인 실패
			else m_PanelText.text = $"로그인 실패!\n{response.context}";
		});

		Dispatcher.Enqueue(() => 
		{
			// 패널 비활성화
			m_BlockingPanel.SetActive(false);

			// 로그인 성공 시, 게임 씬으로 전환시킵니다.
			if (response.result) 
				SceneManager.instance.LoadScene(Game.Constants.SCENENAME_GAME);
		}, 2.0f);
	}

	private void OnLoginButtonClicked()
	{
		string id = m_IDInputField.text;
		string pw = m_PWInputField.text;

		LoginRequestPacket loginRequest = new(id, pw);

		bool failed = false;

		if (!loginRequest.IsLoginable(out string context))
			failed = true;
		else context = "로그인 요청중입니다...";

		m_PanelText.text = context;
		m_BlockingPanel.SetActive(true);

		// 요청 실패
		if (failed) Dispatcher.Enqueue(() => m_BlockingPanel.SetActive(false), 2.0f);
		// 요청 성공
		else NetworkManager.instance.SendPacket(PacketType.LoginRequest, loginRequest);
	}

	private void OnCreateAccountButtonClicked()
	{
		// 서버와 연결되지 않으면 버튼 이벤트 실행 중단
		if (!NetworkManager.instance.connectedServerSocket.Connected) return;



		// 로그인 패널 비활성화
		gameObject.SetActive(false);

		// 회원가입 패널 활성화
		m_CreateAccountPanel.gameObject.SetActive(true);
	}

}
