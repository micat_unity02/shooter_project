using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public sealed class GamePlayerCharacter : PlayerCharacterBase
{
	[SerializeField]
	private Transform _CameraFollowTargetTransform;

	/// <summary>
	/// 닉네임 HUD 객체
	/// </summary>
	private NicknameHUD _NicknameHUD;

	private Vector3 _FollowOffset;

	public GamePlayerCharacterMovement movement { get; private set; }

	public Transform cameraFollowTargetTransform => _CameraFollowTargetTransform;



	/// <summary>
	/// 로컬 캐릭터임을 나타냅니다.
	/// </summary>
	public bool isLocal { get; private set; }

	private Vector3 _InputDirection;

	private void Awake()
	{
		movement = GetComponent<GamePlayerCharacterMovement>();

		_FollowOffset = _CameraFollowTargetTransform.position - transform.position;

		// 캐릭터 오브젝트와 분리시킵니다.
		_CameraFollowTargetTransform.SetParent(null);
	}

	private void Update()
	{
		if (_CameraFollowTargetTransform)
		{
			_CameraFollowTargetTransform.position = transform.position + _FollowOffset;
		}
	}


	public override void OnControlStarted(PlayerControllerBase playerController)
	{
		base.OnControlStarted(playerController);

		Debug.Log("OnControlStarted");

		// 로컬 캐릭터 설정
		isLocal = (playerController as GamePlayerController).isLocal;

		// 자신의 캐릭터가 아니라면
		if (!isLocal)
		{
			Destroy(_CameraFollowTargetTransform.gameObject);
		}
	}

public void SetNicknameHUD(NicknameHUD nicknameHUD)
	{
		_NicknameHUD = nicknameHUD;
	}

	public void OnMoveInput(Vector2 inputAxis)
	{
		if (isLocal) movement.OnMoveInput(inputAxis);
	}


}
