using Core;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor.Search;
using UnityEngine;

public sealed class GameSceneInstance : SceneInstanceBase
{
	[Header("# 플레이어 캐릭터 Prefab")]
	[SerializeField]
	private GamePlayerCharacter _GamePlayerCharacterPrefab;

	[Header("# GameSceneUI")]
	public GameSceneUI m_GameSceneUI;

	private bool _IsOtherUserPacketReceived;

	private Dictionary<string /*nickname*/, GamePlayerCharacter /*character*/> _Characters = new();
	private Dictionary<string /*nickname*/, GamePlayerCharacter /*character*/> characters
	{
		get
		{
			lock(_Characters)
			{
				return _Characters;
			}
		}
	}

	/// <summary>
	/// 생성을 기다리는 캐릭터의 닉네임들을 담기 위한 큐
	/// </summary>
	private Queue<string> _AwaitingForGeneration = new Queue<string>();
	private Queue<string> awaitingForGeneration
	{
		get 
		{ 
			lock(_AwaitingForGeneration)
			{
				return _AwaitingForGeneration;
			}
		}
	}

	private Queue<string> _AwaitingForDestroy = new();
	public Queue<string> awaitingForDestroy
	{
		get
		{
			lock(_AwaitingForDestroy)
			{
				return _AwaitingForDestroy;
			}
		}
	}

	protected override void Awake()
	{
		base.Awake();


		// 로컬에서 생성된 자신의 플레이어 컨트롤러를 얻습니다.
		GamePlayerController localPlayerController = playerController as GamePlayerController;

		// 로컬 캐릭터 생성
		GamePlayerCharacter localPlayerCharacter = CreateGamePlayerCharacter(
			NetworkManager.instance.id);

		// 로컬 플레이어 컨트롤러 시작됨
		localPlayerController.OnLocalPlayerControllerStarted();

		// 로컬 캐릭터 조종 시작
		localPlayerController.StartControlCharacter(localPlayerCharacter);

		// NewPlayerConnected 패킷에 대한 이벤트 등록
		NetworkManager.instance.AddPacketEvent(
			PacketType.NewPlayerConnected, OnNewPlayerConnected);

		// PlayerDisconnected 패킷에 대한 이벤트 등록
		NetworkManager.instance.AddPacketEvent(
			PacketType.PlayerDisconnected, OnOtherPlayerDisconnected);

		// ConnectToWorldResponse 패킷에 대한 이벤트 등록
		NetworkManager.instance.AddPacketEvent(
			PacketType.ConnectToWorldResponse, OnConnectedToWorld);

		// UpdatedPlayerPositionResponse 패킷에 대한 이벤트 등록
		NetworkManager.instance.AddPacketEvent(
			PacketType.UpdatedPlayerPositionResponse, OnPlayerPositionsUpdated);



		// 이 플레이어가 연결되었음을 서버에 알립니다.
		NetworkManager.instance.SendPacket<SimpleRequestPacket>(
			PacketType.ConnectToWorldRequest, new());
	}

	private void Update()
	{
		// 플레이어 캐릭터 생성
		GeneratePalyerCharacterProcedure();

		// 플레이어 캐릭터 제거
		RemovePlayerCharacterProcedure();
	}

	private void OnDestroy()
	{
		NetworkManager.instance.RemovePacketEvent(
			PacketType.NewPlayerConnected, OnNewPlayerConnected);
		NetworkManager.instance.RemovePacketEvent(
			PacketType.ConnectToWorldRequest, OnConnectedToWorld);
		NetworkManager.instance.RemovePacketEvent(
			PacketType.PlayerDisconnected, OnOtherPlayerDisconnected);
		NetworkManager.instance.RemovePacketEvent(
			PacketType.UpdatedPlayerPositionResponse, OnPlayerPositionsUpdated);
	}

	private void OnConnectedToWorld(PacketType packetType, object data)
	{
		NetworkManager.instance.RemovePacketEvent(
			PacketType.ConnectToWorldRequest, OnConnectedToWorld);
		Debug.Log("연결되었음!");


		string nickname = NetworkManager.instance.nickname;


		NetworkManager.instance.AddPacketEvent(
			PacketType.PlayerListResponse, OnPlayerListReceived);

		NetworkManager.instance.SendPacket<PlayerListRequestPacket>(
			PacketType.PlayerListRequest, new(nickname));
		Debug.Log("플레이어 리스트 요청함!");
	}

	private void OnPlayerListReceived(PacketType packetType, object data)
	{
		NetworkManager.instance.RemovePacketEvent(
			PacketType.PlayerListResponse, OnPlayerListReceived);

		Dispatcher.Enqueue(() =>
		{
			PlayerListResponsePacket packet = (PlayerListResponsePacket)data;
			foreach(string nickname in packet.playerList)
			{
				CreateOtherPlayerCharacter(nickname);
			}
		});
	}

	/// <summary>
	/// 새로운 플레이어가 접속한 경우
	/// </summary>
	/// <param name="packetType"></param>
	/// <param name="data"></param>
	private void OnNewPlayerConnected(PacketType packetType, object data)
	{
		NewPlayerConnectionPacket packet = (NewPlayerConnectionPacket)data;

		// 접속한 캐릭터를 생성합니다.
		CreateOtherPlayerCharacter(packet.nickname);
	}

	private void OnOtherPlayerDisconnected(PacketType packetType, object data)
	{
		PlayerDisconnectedPacket packet = (PlayerDisconnectedPacket)data;

		string nickname = packet.nickname;
		if (string.IsNullOrEmpty(nickname)) return;

		// 제거 목록에 존재하지 않는 경우 목록에 추가
		if (!awaitingForDestroy.Contains(nickname))
			awaitingForDestroy.Enqueue(nickname);
	}

	private void OnPlayerPositionsUpdated(PacketType packetType, object data)
	{
		UpdatedPlayerPositionsPacket packet = (UpdatedPlayerPositionsPacket)data;

		Dispatcher.Enqueue(() =>
		{
			foreach(UpdatedPlayerPosition playerPosition in packet.playerPositions)
			{
				// 갱신시킬 캐릭터 닉네임을 얻습니다.
				string nickname = playerPosition.nickname;

				// 제거 대상인 경우 무시
				if (awaitingForDestroy.Contains(nickname)) continue;

				if (characters.TryGetValue(nickname, out GamePlayerCharacter character))
				{
					// 목표 위치, 목표 앞 방향 설정
					character.movement.SetTargetPositionAndDirection(
						playerPosition.position.ToVector3(),
						playerPosition.forward.ToVector3());
				}
			}
		});

	}


	/// <summary>
	/// 다른 플레이어의 캐릭터를 생성합니다.
	/// </summary>
	/// <param name="otherPlayerNickname"></param>
	private void CreateOtherPlayerCharacter(string otherPlayerNickname)
	{
		// 생성시킬 플레이어의 닉네임을 큐에 담습니다.
		awaitingForGeneration.Enqueue(otherPlayerNickname);
	}

	/// <summary>
	/// 플레이어 캐릭터를 생성합니다.
	/// </summary>
	private void GeneratePalyerCharacterProcedure()
	{
		// 생성 요청된 캐릭터가 존재하지 않을 경우
		if (awaitingForGeneration.Count == 0) return;

		// 생성 요청된 캐릭터의 닉네임을 얻습니다.
		string nickname = awaitingForGeneration.Dequeue();

		// 캐릭터 생성
		CreateGamePlayerCharacter(nickname);
	}

	/// <summary>
	/// 접속을 종료한 플레이어 캐릭터를 제거합니다.
	/// </summary>
	private void RemovePlayerCharacterProcedure()
	{
		if (awaitingForDestroy.Count == 0) return;

		string nickname = awaitingForDestroy.Peek();
		if (string.IsNullOrEmpty(nickname))
		{
			// 요청 큐에서 요소를 제거합니다.
			awaitingForDestroy.Dequeue();
			return;
		}

		// 제거 요청된 캐릭터가 생성 대기중 큐에 존재하는 경우 생성 큐에서 제거될 때까지 대기합니다.
		if (awaitingForGeneration.Contains(nickname)) return;

		// 요청 큐에서 요소를 제거합니다.
		awaitingForDestroy.Dequeue();

		// 맵에 생성된 캐릭터를 제거합니다.
		if (characters.TryGetValue(nickname, out GamePlayerCharacter destroyTarget))
		{
			// Dictionary 에서 요소 제거
			characters.Remove(nickname);

			// 플레이어 캐릭터 오브젝트 제거
			Destroy(destroyTarget.gameObject);
		}
	}


	/// <summary>
	/// GamePlayerCharacter 오브젝트를 생성합니다.
	/// </summary>
	/// <param name="nickname">유저의 닉네임을 전달합니다.</param>
	/// <returns>생성된 GamePlayerCharacter 오브젝트를 반환합니다.</returns>
	private GamePlayerCharacter CreateGamePlayerCharacter(string nickname)
	{
		// 이미 캐릭터가 생성된 경우, 함수 호출 종료
		if (characters.ContainsKey(nickname)) return null;

		GamePlayerCharacter newCharacter = Instantiate(_GamePlayerCharacterPrefab);

		// 오브젝트 이름 설정
		newCharacter.gameObject.name = $"{newCharacter.gameObject.name}_{nickname}";

		// 닉네임 HUD 생성
		NicknameHUD nicknameHUD = m_GameSceneUI.nicknamePanel.CreateNicknameHUD(nickname);
		newCharacter.SetNicknameHUD(nicknameHUD);

		characters.Add(nickname, newCharacter);

		return newCharacter;
	}
}
