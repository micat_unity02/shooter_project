using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : GameManagerBase
{
	protected override void OnGameManagerInitialized()
	{
		//base.OnGameManagerInitialized();

		// 씬 매니저
		RegisterManager<SceneManager>();

		// 네트워크 매니저
		RegisterManager<NetworkManager>();
	}





}
