using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.UI;
using UnityEngine.TextCore.Text;

/// <summary>
/// GameScene에서 사용되는 PlayerController 입니다.
/// </summary>
public sealed class GamePlayerController : PlayerControllerBase
{
	private Camera _Camera;
	private Transform _FollowTargetTransform;

	[Header("# Camera")]
	[SerializeField]
	private float _FollowSpeed;

	/// <summary>
	/// 로컬 (자신의) 컨트롤러임을 나타냅니다.
	/// </summary>
	public bool isLocal { get; private set; }

	public GamePlayerCharacter character { get; private set; }

	/// <summary>
	/// 캐릭터 위치를 기록시킬 변수입니다.
	/// </summary>
	private Vector3 _CharacterPosition;

	/// <summary>
	/// 캐릭터의 앞 방향을 기록시킬 변수입니다.
	/// </summary>
	private Vector3 _CharacterForwardDirection;




	/// <summary>
	/// 로컬 플레이어 컨트롤러가 생성되었을 때 호출됩니다.
	/// </summary>
	public void OnLocalPlayerControllerStarted()
	{
		isLocal = true;

		string nickname = NetworkManager.instance.nickname;
	}

	public override void StartControlCharacter(PlayerCharacterBase controlCharacter)
	{
		base.StartControlCharacter(controlCharacter);
		character = controlCharacter as GamePlayerCharacter;

		if (character == null) return;


		StartCoroutine(PositionUpdateRequestRoutine());

		_FollowTargetTransform = character.cameraFollowTargetTransform;
	}

	private void Awake()
	{
		_Camera = Camera.main;
	}

	private void Update()
	{
		if (character == null) return;

		GameSceneInstance gameSceneInstance =
			SceneManager.instance.GetSceneInstance<GameSceneInstance>();

		// UI 객체에 플레이어 위치 갱신
		GameSceneUI gameSceneUI = gameSceneInstance.m_GameSceneUI;

		UpdateCharacterPositionToUI(gameSceneUI);
		FollowCamera();

		// 커서를 바라보는 방향을 얻습니다.
		Vector3 lookDirection = gameSceneUI.lookDirection;

		// 앞 방향을 설정합니다.
		character.movement.SetForwardDirection(lookDirection);
	}

	private void UpdateCharacterPositionToUI(GameSceneUI gameSceneUI)
	{
		gameSceneUI.UpdatePlayerCharacterPosition(
			character.transform.position);
	}

	private void FollowCamera()
	{
		Vector3 cameraPosition = _Camera.transform.position;
		Vector3 targetPosition = _FollowTargetTransform.position;

		Vector3 nextPosition = Vector3.Lerp(
			cameraPosition, targetPosition, _FollowSpeed * Time.deltaTime);

		_Camera.transform.position = nextPosition;
	}

	/// <summary>
	/// 위치 갱신 요청을 보냅니다.
	/// </summary>
	/// <returns></returns>
	private IEnumerator PositionUpdateRequestRoutine()
	{

		// 0.5초 대기
		WaitForSecondsRealtime wait05 = new WaitForSecondsRealtime(0.5f);

		// 위치가 변경될 때까지 대기합니다.
		WaitUntil waitForPositionChanged = new WaitUntil(() =>
			Vector3.Distance(_CharacterPosition, character.transform.position) > 0.05 ||
			_CharacterForwardDirection != character.transform.forward);

		string nickname = NetworkManager.instance.nickname;

		// 유효한 캐릭터가 확인될 때까지 대기합니다.
		yield return new WaitUntil(() => character != null);

		while (true)
		{
			Vector3 position = character.transform.position;
			Vector3 forward = character.transform.forward;

			UpdatePlayerPositionRequestPacket updatePositionRequestPacket =
				new UpdatePlayerPositionRequestPacket(
					nickname, position.ToVector3D(), forward.ToVector3D());

			NetworkManager.instance.SendPacket(
				PacketType.UpdatePlayerPositionRequest, updatePositionRequestPacket);

			// 현재 위치 기록
			_CharacterPosition = character.transform.position;

			// 현재 앞 방향 기록
			_CharacterForwardDirection = character.transform.forward;

			Debug.Log("위치 갱신 요청 보냄!");

			// 0.5초 대기
			yield return wait05;

			// 위치가 변경될 때까지 대기합니다.
			yield return waitForPositionChanged;
		}
	}


	private void OnMove(InputValue value)
	{
		Vector2 axisValue = value.Get<Vector2>();
		character?.OnMoveInput(axisValue);
	}

}
